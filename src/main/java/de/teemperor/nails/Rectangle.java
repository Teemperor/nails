/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.teemperor.nails;

/**
 *
 * @author teemperor
 */
public class Rectangle {

    private final int x1;
    private final int y1;
    private final int x2;
    private final int y2;

    public Rectangle(int x, int y, int w, int h) {
        this.x1 = x;
        this.y1 = y;
        this.x2 = x + w;
        this.y2 = y + h;
    }

    public boolean overlapsWith(Rectangle r) {
        return !(x1 >= r.getX2() || x2 <= r.getX1() || y1 >= r.getY2() || y2 <= r.getY1());
    }

    public boolean containsPoint(int x, int y) {
        return x >= x1 && x <= x2 && y >= y1 && y <= y2;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }
}
