package de.teemperor.nails;

import de.teemperor.nails.scriptnodes.NodeShopArea;
import de.teemperor.nails.scriptnodes.ScriptNode;
import de.teemperor.nails.scriptnodes.NodeAnnounceText;
import de.teemperor.nails.scriptnodes.NodeLevelExit;
import de.teemperor.nails.scriptnodes.NodeAreaTrigger;
import de.teemperor.nails.scriptnodes.NodeToggleElement;
import de.teemperor.nails.scriptnodes.NodeObjectEventTrigger;
import de.teemperor.nails.Doodad.DoodadType;
import de.teemperor.nails.Item.ItemType;
import de.teemperor.nails.scriptnodes.ScriptNode.NodeType;
import java.util.ArrayList;

public class ObjectSet {

    public enum SetType {

        ExitUp,
        ExitDn,
        Shop,
        Orb,
        RestoreOrb
    }

    private ArrayList<Doodad> doodads;
    private ArrayList<ScriptNode> scriptNodes;
    private ArrayList<Item> items;
    private SetType type;
    private int x;
    private int y;
    private int width;
    private int height;
    private int wallX;
    private int wallY;
    private int wallWidth;
    private int wallHeight;
    private boolean replaceWalls;

    private Level level;

    public ObjectSet(Level level, int x, int y, SetType type, String theme) {
        this.level = level;
        ScriptNode shape;

        doodads = new ArrayList<>();
        scriptNodes = new ArrayList<>();
        items = new ArrayList<>();

        this.x = x;
        this.y = y;
        this.type = type;

        switch (type) {

            case ExitUp:
                doodads.add(level.createDoodad(x + 1, y + 1, DoodadType.TDown, theme));
                doodads.add(level.createDoodad(x + 4, y + 1, DoodadType.TDown, theme));
                doodads.add(level.createDoodad(x + 1, y + 3, DoodadType.TorchOff, theme));
                doodads.add(level.createDoodad(x + 2, y + 3, DoodadType.ExitUp, theme));
                doodads.add(level.createDoodad(x + 4, y + 3, DoodadType.TorchOff, theme));
                doodads.add(level.createDoodad(x + 1.5f, y + 0.25f, DoodadType.Cover, theme));
                doodads.add(level.createDoodad(x + 2.5f, y + 0.25f, DoodadType.Cover, theme));
                doodads.add(level.createDoodad(x + 2, y + 4, DoodadType.ExitMarker, theme));
                scriptNodes.add(level.createScriptNode(x + 3, y + 5, NodeType.LevelStart));

                shape = level.createScriptNode(x + 3, y + 5, NodeType.RectangleShape);
                scriptNodes.add(shape);

                NodeAreaTrigger areaTrig = (NodeAreaTrigger) level.createScriptNode(x + 3, y + 6, NodeType.AreaTrigger);
                areaTrig.connectToShape(shape);
                scriptNodes.add(areaTrig);

                NodeAnnounceText levelText = (NodeAnnounceText) level.createScriptNode(x + 3, y + 7, NodeType.AnnounceText);
                levelText.setText("Level " + (Level.currentLevel + 1), 0);
                areaTrig.connectTo(levelText);
                scriptNodes.add(levelText);

                NodeToggleElement toggle = (NodeToggleElement) level.createScriptNode(x + 3, y + 8, NodeType.ToggleElement);
                toggle.connectToElement(areaTrig);
                areaTrig.connectTo(toggle);
                scriptNodes.add(toggle);

                //NodeAreaTrigger areaT = (NodeAreaTrigger) ScriptNode.Create( x + 3, y + 6, NodeType.AreaTrigger );
                //areaT.connectToShape(shape);
                //scriptNodes.add( areaT );
                ScriptNode resScript = level.createScriptNode(x, y + 8, NodeType.RespawnPlayers);
                areaTrig.connectTo(resScript);
                scriptNodes.add(resScript);
                width = 6;
                height = 5;
                wallWidth = 3;
                wallHeight = 4;
                wallX = x + 1;
                wallY = y + 1;
                replaceWalls = true;
                break;

            case ExitDn:
                doodads.add(level.createDoodad(x + 1, y + 1, DoodadType.TDown, theme));
                doodads.add(level.createDoodad(x + 4, y + 1, DoodadType.TDown, theme));
                doodads.add(level.createDoodad(x + 1, y + 3, DoodadType.Torch, theme));
                doodads.add(level.createDoodad(x + 2, y + 3, DoodadType.ExitDn, theme));
                doodads.add(level.createDoodad(x + 4, y + 3, DoodadType.Torch, theme));
                doodads.add(level.createDoodad(x + 1.5f, y + 0.25f, DoodadType.Cover, theme));
                doodads.add(level.createDoodad(x + 2.5f, y + 0.25f, DoodadType.Cover, theme));
                doodads.add(level.createDoodad(x + 2, y + 4, DoodadType.ExitMarker, theme));

                shape = level.createScriptNode(x + 3, y + 4, NodeType.RectangleShape);
                scriptNodes.add(shape);

                NodeLevelExit exit = (NodeLevelExit) level.createScriptNode(x + 3, y + 6, NodeType.LevelExit);
                exit.connectToShape(shape);
                scriptNodes.add(exit);

                width = 6;
                height = 5;
                wallWidth = 3;
                wallHeight = 4;
                wallY = y + 1;
                wallX = x + 1;
                replaceWalls = true;
                break;

            case Shop:
                shape = level.createScriptNode(x, y, NodeType.RectangleShape);
                scriptNodes.add(shape);
                NodeShopArea shop = (NodeShopArea) level.createScriptNode(x, y, NodeType.ShopArea);
                shop.connectToShape(shape);
                scriptNodes.add(shop);
                doodads.add(level.createDoodad(x, y, shop.getShopType().vendor, theme));
                width = 1;
                height = 1;
                replaceWalls = false;
                break;

            case Orb:
                Item orb = level.createItem(x, y, ItemType.Orb, 0);
                items.add(orb);

                NodeObjectEventTrigger trigger = (NodeObjectEventTrigger) level.createScriptNode(x, y + 2, NodeType.ObjectEventTrigger);
                trigger.connectItem(orb);

                NodeAnnounceText textScript = (NodeAnnounceText) level.createScriptNode(x, y + 4, NodeType.AnnounceText);
                trigger.connectTo(textScript);

                scriptNodes.add(trigger);
                scriptNodes.add(textScript);

                width = 1;
                height = 1;
                replaceWalls = false;
                break;

            case RestoreOrb: //unused
                Item orbR = level.createItem(x, y, ItemType.Orb, 1);
                items.add(orbR);

                NodeObjectEventTrigger triggerR = (NodeObjectEventTrigger) level.createScriptNode(x, y + 2, NodeType.ObjectEventTrigger);
                triggerR.connectItem(orbR);

                NodeAnnounceText titleScript = (NodeAnnounceText) level.createScriptNode(x, y + 4, NodeType.AnnounceText);
                titleScript.setText("Orb of Restoration", 0);
                triggerR.connectTo(titleScript);

                NodeAnnounceText subScript = (NodeAnnounceText) level.createScriptNode(x, y + 6, NodeType.AnnounceText);
                subScript.setText("Fallen party members restored", 1);
                triggerR.connectTo(subScript);

                //ScriptNode resScript = ScriptNode.Create( x, y + 8, NodeType.RespawnPlayers );
                //triggerR.connectTo( resScript );
                scriptNodes.add(triggerR);
                scriptNodes.add(titleScript);
                scriptNodes.add(subScript);
                //scriptNodes.add( resScript );

                width = 1;
                height = 1;
                replaceWalls = false;
                break;
        }
    }

    public int getX() {
        return x;
    }

    public int getWidth() {
        return width;
    }

    public boolean shouldReplaceWalls() {
        return replaceWalls;
    }

    public boolean contains(int x, int y) {

        return x <= this.x + this.width
                && x >= this.x
                && y <= this.y + this.height
                && y >= this.y;
    }

    public boolean containsWall(int x, int y) {

        return x <= this.wallX + this.wallWidth
                && x >= this.wallX
                && y <= this.wallY + this.wallHeight
                && y >= this.wallY;
    }
}
