package de.teemperor.nails;

import de.teemperor.nails.config.Parameters;
import de.teemperor.nails.Item.ItemType;
import de.teemperor.nails.Monster.MonsterType;
import de.teemperor.nails.ObjectSet.SetType;

public class Room {

    public enum RoomType {

        None, Entrance, Exit, Vault, Lair, Storage, Shop, Orb
    }

    private final int x;
    private final int y;
    private final int width;
    private final int height;
    private RoomType type;
    private final boolean flag;
    private final String theme;
    private MonsterType monsterType;
    private final Rectangle coll_rect;

    private final Level level;

    public Room(Level level) {
        this.level = level;
        // perform random generation
        width = Rand.iRand(Parameters.minRoomSize, Parameters.maxRoomSize);
        height = Rand.iRand(Parameters.minRoomSize + 2,
                Parameters.maxRoomSize + 2);
        x = Rand.iRand(Parameters.edgePadding, Parameters.mapWidth
                - Parameters.edgePadding - width);
        y = Rand.iRand(Parameters.edgePadding, Parameters.mapHeight
                - Parameters.edgePadding - height);
        type = RoomType.None;
        flag = false;
        this.theme = Parameters.themes[level.getIndex()];
        coll_rect = new Rectangle(getX(), getY(), getWidth(), getHeight());
    }

    public boolean overlap(Room otherRoom) {
        return coll_rect.overlapsWith(otherRoom.getCollisionRectangle());
    }

    public Rectangle getCollisionRectangle() {
        return coll_rect;
    }

    public boolean contains(int x, int y) {
        return coll_rect.containsPoint(x, y);
    }

    public boolean transform(RoomType type) {

        float area = getWidth() * getHeight();
        int spawners, foodDrops;
        if (this.type != RoomType.None) {
            return false;
        }

        createBreakables();

        switch (type) {

            case Entrance:
            case Exit:
                SetType setType;
                if (type == RoomType.Entrance) {
                    setType = SetType.ExitUp;
                } else {
                    setType = SetType.ExitDn;
                }
                for (int attempt = 0; attempt < 20; attempt++) {

                    boolean safe = false;
                    ObjectSet s = level.createObjectSet(Rand.iRand(getX(), getX() + getWidth()), getY() - 2,
                            setType, theme);
                    if (s.getX() + s.getWidth() < getX() + getWidth()) {

                        if (type == RoomType.Exit) {
                            // ObjectSet.Create( x + width / 2, y + height / 2 +
                            // 1, SetType.RestoreOrb, theme );
                            this.transform(RoomType.Lair);
                            this.type = type;
                            return true;
                        } else {
                            this.transform(RoomType.Storage);
                            this.type = type;
                            return true;
                        }

                    }
                }
                return false;

            case Lair:

                monsterType = Monster.chooseMonsterForLevel(level.getIndex());

                CreateHorde(
                        monsterType,
                        (int) (Rand.fRand(
                                Parameters.monsterCounts[monsterType.ordinal()] / 5,
                                Parameters.monsterCounts[monsterType.ordinal()]) * Parameters.monsterMultiplier));
                this.type = type;

                spawners = Rand.iRand(0,
                        Parameters.monsterCounts[monsterType.ordinal()] / 20);
                for (int i = 0; i < spawners; i++) {

                    level.createMonster(Rand.fRand(getX() + 2, getX() + getWidth() - 2),
                            Rand.fRand(getY() + 4, getY() + getHeight() - 2), monsterType, 0);
                }

                int treasurePiles = Rand.iRand(1, 4);
                for (int i = 0; i < treasurePiles; i++) {

                    CreateTreasure((int) (Rand.fRand(area / 20, area / 6) * Parameters.goldMultiplier));
                }

                foodDrops = Rand.iRand(0, 2);
                for (int i = 0; i < foodDrops; i++) {

                    CreateFood((int) (Rand.fRand(2, 6) * Parameters.foodMultiplier));
                }

                return true;

            case Storage:

                monsterType = Monster.chooseMonsterForLevel(level.getIndex());

                this.type = type;

                spawners = Rand.iRand(0, 3);
                for (int i = 0; i < spawners; i++) {

                    level.createMonster(Rand.fRand(getX() + 1, getX() + getWidth() - 1),
                            Rand.fRand(getY() + 3, getY() + getHeight() - 1), monsterType, 0);
                }

                foodDrops = Rand.iRand(1, 3);
                for (int i = 0; i < foodDrops; i++) {

                    CreateFood((int) (Rand.fRand(2, 6) * Parameters.foodMultiplier));
                }
                return true;

            case Shop:
                level.createObjectSet(getX() + getWidth() / 2, getY() + getHeight() / 2 + 1, SetType.Shop,
                        theme);

                this.type = type;
                return true;

            case Vault:
                if (!lockRoom()) {
                    return false;
                }

                treasurePiles = Rand.iRand(6, 8);
                for (int i = 0; i < treasurePiles; i++) {

                    CreateTreasure((int) (Rand.fRand(area / 12, area / 8) * Parameters.goldMultiplier));
                }
                this.type = type;
                return true;

            case Orb:
                level.createObjectSet(getX() + getWidth() / 2, getY() + getHeight() / 2 + 1, SetType.Orb,
                        theme);

                this.type = type;
                return true;

            default:
                this.type = type;
                return true;
        }

    }

    public int leftFreeX() {
        return x;
    }

    public int rightFreeX() {
        return x + width + 1;
    }

    public int topFreeY() {
        return y + 2;
    }

    public int bottomFreeY() {
        return y + height + 1;
    }

    public void CreateHorde(Monster.MonsterType type, int count) {

        float originX, originY, radius;

        originX = Rand.fRand(getX() + 2, getX() + getWidth() - 2);
        originY = Rand.fRand(getY() + 4, getY() + getHeight() - 2);
        radius = Rand.fRand(5, 15);

        float driftAngle = Rand.fRand(0, 2 * (float) Math.PI);
        float curve = Rand.fRand(0, 0.5f) - 0.25f;
        float drift = 6.0f / count;

        for (int i = 0; i < count; i++) {
            float angle = Rand.fRand(0, 2 * (float) Math.PI);
            float r = Rand.fRand(0, radius);
            float mx = originX + r * (float) Math.cos((double) angle);
            float my = originY + r * (float) Math.sin((double) angle);
            if (mx > getX() + getWidth()) {
                continue;
            } else if (mx < getX()) {
                continue;
            }
            if (my > getY() + getHeight()) {
                continue;
            } else if (my < getY() + 2) {
                continue;
            }
            level.createMonster(mx, my, type);

            originX += drift * (float) Math.cos((double) driftAngle);
            originY += drift * (float) Math.sin((double) driftAngle);
            driftAngle += curve;
        }
    }

    public void CreateTreasure(int count) {

        float originX, originY, radius;
        originX = Rand.fRand(getX() + 2, getX() + getWidth() - 2);
        originY = Rand.fRand(getY() + 4, getY() + getHeight() - 2);
        radius = Rand.fRand(2, 6);

        float driftAngle = Rand.fRand(0, 2 * (float) Math.PI);
        float curve = Rand.fRand(0, 0.5f) - 0.25f;
        float drift = 10.0f / count;

        for (int i = 0; i < count; i++) {
            float angle = Rand.fRand(0, 2 * (float) Math.PI);
            float r = Rand.fRand(0, radius);
            float mx = originX + r * (float) Math.cos((double) angle);
            float my = originY + r * (float) Math.sin((double) angle);
            if (mx > getX() + getWidth()) {
                continue;
            } else if (mx < getX()) {
                continue;
            }
            if (my > getY() + getHeight()) {
                continue;
            } else if (my < getY() + 2) {
                continue;
            }

            int treasureIndex = Rand.iRand(0, 5);
            treasureIndex += 2;
            treasureIndex = Math.min(treasureIndex,
                    ItemType.Treasure.typeStrings.length - 1);

            level.createItem(mx, my, ItemType.Treasure, treasureIndex);

            originX += drift * (float) Math.cos((double) driftAngle);
            originY += drift * (float) Math.sin((double) driftAngle);
            driftAngle += curve;
        }
    }

    public void createBreakables() {
        int corner = Rand.iRand(0, 3);
        float originX = 0, originY = 0;

        boolean flipX = false;
        boolean flipY = false;

        switch (corner) {
            case 0: //upper left
                originX = leftFreeX();
                originY = topFreeY();
                break;
            case 1: //upper right
                originX = rightFreeX();
                originY = topFreeY();
                flipX = true;
                break;
            case 2: //lower right
                originX = rightFreeX();
                originY = bottomFreeY();
                flipY = true;
                flipX = true;
                break;
            case 3: //lower left
                originX = leftFreeX();
                originY = bottomFreeY();
                flipY = true;
                break;
        }

        for (float run_x = 0.3f; run_x <= 6; run_x += 0.5f) {
            float limit = (Rand.fRand(1, 4) * 1.0f / (run_x + 1));
            for (float run_y = 0; run_y <= limit; run_y += 0.5f) {
                level.createItem(originX + (flipX ? -run_x : run_x),
                        originY + (flipY ? -run_y : run_y),
                        ItemType.Breakable);
            }
        }

    }

    public void CreateFood(int count) {

        float originX, originY, radius;
        originX = Rand.fRand(getX() + 2, getX() + getWidth() - 2);
        originY = Rand.fRand(getY() + 4, getY() + getHeight() - 2);
        radius = Rand.fRand(2, 4);

        float driftAngle = Rand.fRand(0, 2 * (float) Math.PI);
        float curve = Rand.fRand(0, 0.5f) - 0.25f;
        float drift = 10.0f / count;

        for (int i = 0; i < count; i++) {
            float angle = Rand.fRand(0, 2 * (float) Math.PI);
            float r = Rand.fRand(0, radius);
            float mx = originX + r * (float) Math.cos((double) angle);
            float my = originY + r * (float) Math.sin((double) angle);
            if (mx > getX() + getWidth()) {
                continue;
            } else if (mx < getX()) {
                continue;
            }
            if (my > getY() + getHeight()) {
                continue;
            } else if (my < getY() + 2) {
                continue;
            }

            level.createItem(mx, my, ItemType.Food);

            originX += drift * (float) Math.cos((double) driftAngle);
            originY += drift * (float) Math.sin((double) driftAngle);
            driftAngle += curve;
        }
    }

    public boolean lockRoom() {

        /* if (locked || type == RoomType.Entrance
         || type == RoomType.Exit || type == RoomType.Orb) {
         return false;
         }

         locked = true;
         int lockTier = Rand.iRand(0, 3);

         PosDir entrance = new PosDir();

         PosDir pathPos = p.path.get(0);
         entrance.dir = pathPos.dir;
         entrance.x = pathPos.x;
         entrance.y = pathPos.y;

         switch (entrance.dir) {

         case UP:
         for (int xOffset = 0; xOffset < p.width; xOffset++) { // vertical
         // doors are
         // 2 tiles
         // long

         Item.Create(entrance.x + xOffset + 0.5f, entrance.y,
         ItemType.Door, lockTier);
         }
         break;

         case DOWN:
         for (int xOffset = 0; xOffset < p.width; xOffset++) { // vertical
         // doors are
         // 2 tiles
         // long

         Item.Create(entrance.x + xOffset + 0.5f, entrance.y + 3,
         ItemType.Door, lockTier);
         }
         break;

         case LEFT:
         case RIGHT:
         for (int yOffset = 0; yOffset < p.width + 1; yOffset++) {

         Item.Create(entrance.x + 0.5f, entrance.y + yOffset + 2,
         ItemType.Door, lockTier + 3); // add 3 for vertical
         // doors
         }
         break;
         }
         lastLockType = lockTier;

         // add loot
         Item.Create(Rand.fRand(getX(), getX() + getWidth()), Rand.fRand(getY() + 2, getY() + getHeight()),
         ItemType.Powerup); */
        return true;
    }

    public boolean spawnKey() {
        level.createItem(Rand.fRand(getX(), getX() + getWidth()), Rand.fRand(getY() + 2, getY() + getHeight()),
                ItemType.Key);
        return true;
    }

    /**
     * @return the x
     */
    public final int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public final int getY() {
        return y;
    }

    /**
     * @return the width
     */
    public final int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public final int getHeight() {
        return height;
    }
}
