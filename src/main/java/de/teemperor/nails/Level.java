package de.teemperor.nails;

import de.teemperor.nails.Doodad.DoodadType;
import de.teemperor.nails.config.Parameters;
import de.teemperor.nails.scriptnodes.NodeAnnounceText;
import de.teemperor.nails.scriptnodes.NodeAreaTrigger;
import de.teemperor.nails.scriptnodes.NodeLevelExit;
import de.teemperor.nails.scriptnodes.NodeLevelStart;
import de.teemperor.nails.scriptnodes.NodeObjectEventTrigger;
import de.teemperor.nails.scriptnodes.NodeRectangleShape;
import de.teemperor.nails.scriptnodes.NodeShopArea;
import de.teemperor.nails.scriptnodes.NodeToggleElement;
import de.teemperor.nails.scriptnodes.ScriptNode;
import de.teemperor.nails.xml.XMLArray;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLIntArray;
import de.teemperor.nails.xml.XMLString;
import java.util.ArrayList;
import java.util.LinkedList;

public class Level {

    static final int tilemapSize = 20;
    public static int currentLevel;
    public static int idCounter = 0;

    public enum Tilemap {

        A_Default("tilemaps/a_default.xml", 2),
        B_Default("tilemaps/b_default.xml", 4),
        C_Default("tilemaps/b_default.xml", 2),
        D_Default("tilemaps/b_default.xml", 2);

        public XMLString string;
        public int tiles;

        Tilemap(String name, int tiles) {
            this.string = new XMLString("tileset", name);
            this.tiles = tiles;
        }
    }

    private int index;
    private int width;
    private int height;
    private ArrayList<Room> rooms;
    private Tile[] tileArray;
    private boolean levelValid;
    private String theme;

    private LinkedList<Doodad> doodads = new LinkedList<>();
    private LinkedList<Monster> monsters = new LinkedList<>();
    private LinkedList<Item> items = new LinkedList<>();
    private LinkedList<ObjectSet> sets = new LinkedList<>();
    private LinkedList<ScriptNode> nodes = new LinkedList<>();

    public Level(int index) {
        this.index = index;
        idCounter = 0;
        theme = Parameters.themes[index];
        levelValid = true;
        rooms = new ArrayList<>();
        currentLevel = index;
        width = 200;
        height = 200;

        // generate rooms
        int roomCount = 3;

        for (int i = 0; i < roomCount; i++) {
            for (int attempt = 0; attempt < 1000; attempt++) {

                Room newRoom = new Room(this);
                // check for conflicts with existing rooms
                boolean conflict = false;
                for (Room r : rooms) {

                    if (newRoom.overlap(r)) {
                        conflict = true;
                        break;
                    }
                }
                if (!conflict) {
                    rooms.add(newRoom);
                    break;
                }
            }
        }

        // create special rooms
        // entrance
        boolean success = false;
        for (int attempt = 0; attempt < 2000; attempt++) {
            Room r = rooms.get(Rand.iRand(0, rooms.size()));
            if (r.transform(Room.RoomType.Entrance)) {
                success = true;
                break;
            }
        }

        if (!success) {
            // level generation failed
            levelValid = false;
        }

        // exit
        if (index < Parameters.levels - 1) {
            success = false;
            for (int attempt = 0; attempt < 2000; attempt++) {
                Room r = rooms.get(Rand.iRand(0, rooms.size()));
                if (r.transform(Room.RoomType.Exit)) {
                    success = true;
                    break;
                }
            }

            if (!success) {
                // level generation failed
                levelValid = false;
            }
        } else {

            // final level
            success = false;
            for (int attempt = 0; attempt < 2000; attempt++) {
                Room r = rooms.get(Rand.iRand(0, rooms.size()));
                if (r.transform(Room.RoomType.Orb)) {
                    success = true;
                    break;
                }
            }

            if (!success) {
                // level generation failed
                levelValid = false;
            }

        }

        // build level data
        buildTileArray();
        buildWalls();
    }

    public int getIndex() {
        return index;
    }

    public boolean isLevelValid() {
        return levelValid;
    }

    // generate XML structure for level
    public String getXML() {

        // tile maps
        XMLArray tiledataArray = new XMLArray("tiledata");

        // create 20x20 blocks of tilemaps
        int xTiles = (int) Math.ceil((double) Parameters.mapWidth / (double) tilemapSize);
        int yTiles = (int) Math.ceil((double) Parameters.mapHeight / (double) tilemapSize);

        for (int x = 0; x < xTiles + 1; x++) {
            for (int y = 0; y < yTiles + 1; y++) {

                XMLDictionary tileSet = new XMLDictionary("");
                switch (theme) {

                    case "d":
                        tileSet.addData(Tilemap.D_Default.string);
                        break;

                    case "c":
                        tileSet.addData(Tilemap.C_Default.string);
                        break;

                    case "b":
                        tileSet.addData(Tilemap.B_Default.string);
                        break;

                    default:
                        tileSet.addData(Tilemap.A_Default.string);
                        break;
                }

                tileSet.addData(new XMLIntArray("data-t", getTiles(x * tilemapSize, y * tilemapSize, Tilemap.A_Default)));
                tileSet.addData(defaultIntArray("data-r"));
                tileSet.addData(defaultIntArray("data-g"));
                tileSet.addData(defaultIntArray("data-b"));
                tileSet.addData(defaultIntArray("data-a"));

                XMLArray dataSets = new XMLArray("datasets");
                dataSets.addData(tileSet);

                XMLDictionary tileBlock = new XMLDictionary("");
                tileBlock.addData(new XMLInt("x", x * tilemapSize));
                tileBlock.addData(new XMLInt("y", y * tilemapSize));
                tileBlock.addData(dataSets);

                tiledataArray.addData(tileBlock);
            }
        }

        XMLDictionary tilemapDict = new XMLDictionary("tilemap");
        tilemapDict.addData(tiledataArray);

        // doodads
        XMLArray doodadsArray = new XMLArray("doodads");
        for (Doodad d : doodads) {

            doodadsArray.addData(d);
        }

        XMLDictionary doodadsDict = new XMLDictionary("doodads");
        doodadsDict.addData(doodadsArray);

        // actors
        XMLArray actorsArray = new XMLArray("actors");
        for (Monster m : monsters) {

            actorsArray.addData(m);
        }

        XMLDictionary actorsDict = new XMLDictionary("actors");
        actorsDict.addData(actorsArray);

        // items
        XMLArray itemsArray = new XMLArray("items");
        for (Item i : items) {

            itemsArray.addData(i);
        }

        XMLDictionary itemsDict = new XMLDictionary("items");
        itemsDict.addData(itemsArray);

        // scripts
        XMLArray nodesArray = new XMLArray("nodes");
        for (ScriptNode n : nodes) {
            nodesArray.addData(n);
        }

        XMLDictionary scriptingDict = new XMLDictionary("scripting");
        scriptingDict.addData(nodesArray);

        // lighting
        XMLArray lightingArray = new XMLArray("lights");

        XMLDictionary ambientDict = new XMLDictionary("ambient-color");
        ambientDict.addData(new XMLInt("r", 255));
        ambientDict.addData(new XMLInt("g", 255));
        ambientDict.addData(new XMLInt("b", 255));
        ambientDict.addData(new XMLInt("a", 255));

        XMLDictionary shadowDict = new XMLDictionary("shadow-color");
        shadowDict.addData(new XMLInt("r", 128));
        shadowDict.addData(new XMLInt("g", 128));
        shadowDict.addData(new XMLInt("b", 128));
        shadowDict.addData(new XMLInt("a", 128));

        XMLDictionary lightingDict = new XMLDictionary("lighting");
        lightingDict.addData(lightingArray);
        lightingDict.addData(ambientDict);
        lightingDict.addData(shadowDict);

        // create master dictionary
        XMLDictionary masterDict = new XMLDictionary("");
        masterDict.addData(tilemapDict);
        masterDict.addData(doodadsDict);
        masterDict.addData(actorsDict);
        masterDict.addData(scriptingDict);
        masterDict.addData(itemsDict);
        masterDict.addData(lightingDict);

        return masterDict.toXML();
    }

    public ScriptNode createScriptNode(float x, float y, ScriptNode.NodeType type) {

        ScriptNode n;
        switch (type) {

            case LevelStart:
                n = new NodeLevelStart(x, y, type);
                break;

            case LevelExit:
                n = new NodeLevelExit(x, y, type);
                break;

            case RectangleShape:
                n = new NodeRectangleShape(x, y, type);
                break;

            case ShopArea:
                n = new NodeShopArea(x, y, type);
                break;

            case AnnounceText:
                n = new NodeAnnounceText(x, y, type);
                break;

            case ObjectEventTrigger:
                n = new NodeObjectEventTrigger(x, y, type);
                break;

            case ToggleElement:
                n = new NodeToggleElement(x, y, type);
                break;

            case AreaTrigger:
                n = new NodeAreaTrigger(x, y, type);
                break;

            default:
                n = new ScriptNode(x, y, type);
                break;
        }
        nodes.add(n);
        return n;
    }

    public ObjectSet createObjectSet(int x, int y, ObjectSet.SetType type, String theme) {
        ObjectSet s = new ObjectSet(this, x, y, type, theme);
        sets.add(s);
        return s;
    }

    public Item createItem(float x, float y, Item.ItemType type) {
        return createItem(x, y, type, Rand.iRand(0, type.typeStrings.length));
    }

    public Item createItem(float x, float y, Item.ItemType type, int index) {
        Item n = new Item(x, y, type, index);
        items.add(n);
        return n;
    }

    public Doodad createDoodad(float x, float y, DoodadType type, String theme) {
        Doodad d = new Doodad(x, y, type, theme);
        doodads.add(d);
        return d;
    }

    public Monster createMonster(float x, float y, Monster.MonsterType type, int tier) {
        Monster m = new Monster(x, y, type, tier);
        monsters.add(m);
        return m;
    }

    public Monster createMonster(float x, float y, Monster.MonsterType type) {
        // determine tier
        int tier = 1;
        while (Rand.fRand(0, 1) < type.upgradeChance && tier < type.xmlStrings.length - 1) {

            tier++;
        }
        return createMonster(x, y, type, tier);
    }

    private int[] getTiles(int x, int y, Tilemap map) {

        int[] tiles = new int[tilemapSize * tilemapSize];
        for (int i = 0; i < tilemapSize * tilemapSize; i++) {
            int tileX = (x - 10) + i % tilemapSize;
            int tileY = (y - 10) + i / tilemapSize;
            int tileIndex = tileX + tileY * width;
            if (tileIndex >= 0 && tileIndex < width * height && tileX >= 0 && tileX < width && tileY >= 0 && tileY < height && !tileArray[tileIndex].isWall()) {
                tiles[i] = (int) (Math.random() * map.tiles) + 1;
            } else {
                tiles[i] = 0;
            }
        }
        return tiles;
    }

    private XMLIntArray defaultIntArray(String name) {
        XMLIntArray data = new XMLIntArray(name, new int[tilemapSize * tilemapSize]);
        for (int i = 0; i < tilemapSize * tilemapSize; i++) {
            data.getData()[ i] = 255;
        }
        return data;
    }

    private void buildTileArray() {
        tileArray = new Tile[width * height];
        for (int i = 0; i < width * height; i++) {

            tileArray[i] = new Tile(false);

            int x = i % width;
            int y = i / width;

            boolean isWall = true;
            // check rooms
            for (Room r : rooms) {
                if (r.contains(x, y)) {
                    isWall = false;
                    break;
                }
            }

            tileArray[i].setWall(isWall);

            // check for wall replacing object sets
            for (ObjectSet s : sets) {
                if (s.shouldReplaceWalls() && s.containsWall(x, y)) {
                    tileArray[i].setWallSet(true);
                }
            }
        }
    }

    private void buildWalls() {
        for (int i = 0; i < width * height; i++) {

            int x = i % (width);
            int y = i / (width);

            if (tileArray[i].isWallSet()) {
                continue;
            }

            DoodadType type = WallPattern.searchPatterns(x, y, tileArray, width, true);

            if (type != null) {
                // create the new doodad
                createDoodad(x, y, type, theme);

            }

            // check for non-wall doodads, like cover
            type = WallPattern.searchPatterns(x, y, tileArray, width, false);

            if (type != null) {
                // create the new doodad
                createDoodad(x, y, type, theme);
            }
        }
    }
}
