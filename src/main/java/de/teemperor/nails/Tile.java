package de.teemperor.nails;

public class Tile {

    private boolean wall;
    private boolean wallSet = false;

    Tile(boolean wall) {
        this.wall = wall;
    }

    /**
     * @return the wall
     */
    public boolean isWall() {
        return wall;
    }

    /**
     * @return the wallSet
     */
    public boolean isWallSet() {
        return wallSet;
    }

    /**
     * @param wall the wall to set
     */
    public void setWall(boolean wall) {
        this.wall = wall;
    }

    /**
     * @param wallSet the wallSet to set
     */
    public void setWallSet(boolean wallSet) {
        this.wallSet = wallSet;
    }
}
