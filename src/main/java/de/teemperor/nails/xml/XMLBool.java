package de.teemperor.nails.xml;

public class XMLBool extends NamedXMLObject {

    private final boolean value;

    public XMLBool(String name, boolean value) {
        super(name);
        this.value = value;
    }

    @Override
    public String toXML() {

        String valueString = "False";
        if (value) {
            valueString = "True";
        }
        return String.format("<bool name=\"%s\">%s</bool>", getName(), valueString);
    }
}
