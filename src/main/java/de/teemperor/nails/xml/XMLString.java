package de.teemperor.nails.xml;

public class XMLString extends NamedXMLObject {

    private String value;

    public XMLString(String name, String value) {
        super(name);
        this.value = value;
    }

    public void setValue(String newString) {
        value = newString;
    }

    @Override
    public String toXML() {
        return "<string name=\"" + getName() + "\">" + value + "</string>";
    }
}
