/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.teemperor.nails.xml;

/**
 *
 * @author teemperor
 */
public abstract class NamedXMLObject extends XMLObject {

    private final String name;

    public NamedXMLObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
