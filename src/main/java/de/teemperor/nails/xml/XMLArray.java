package de.teemperor.nails.xml;

import java.util.ArrayList;

public class XMLArray extends NamedXMLObject {

    private final ArrayList<XMLObject> dataList;

    public XMLArray(String name) {
        super(name);
        dataList = new ArrayList<>();
    }

    public void addData(XMLObject object) {
        dataList.add(object);
    }

    @Override
    public String toXML() {
        String xmlString = "<array name=\"" + getName() + "\">";
        for (XMLObject d : dataList) {
            xmlString += d.toXML();
        }
        return xmlString + "</array>";
    }
}
