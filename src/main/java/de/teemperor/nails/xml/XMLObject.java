package de.teemperor.nails.xml;

public abstract class XMLObject {

    public abstract String toXML();
}
