package de.teemperor.nails.xml;

import java.util.ArrayList;

public class XMLDictionary extends NamedXMLObject {

    private final ArrayList<XMLObject> dataList;

    public XMLDictionary(String name) {
        super(name);
        dataList = new ArrayList<>();
    }

    public void addData(XMLObject object) {
        dataList.add(object);
    }

    @Override
    public String toXML() {

        String xmlString = String.format("<dictionary");
        if (getName() != null && !getName().isEmpty()) {
            xmlString = xmlString.concat(String.format(" name=\"%s\"", getName()));
        }
        xmlString = xmlString.concat(">\n");
        for (XMLObject d : dataList) {
            xmlString = xmlString.concat(String.format("%s\n", d.toXML()));
        }
        return xmlString.concat("</dictionary>\n");
    }
}
