package de.teemperor.nails.xml;

public class XMLFloat extends NamedXMLObject {

    private float value;

    public XMLFloat(String name, float value) {
        super(name);
        this.value = value;
    }

    public void setValue(int newValue) {
        value = newValue;
    }

    @Override
    public String toXML() {

        return String.format("<float name=\"%s\">%f</float>", getName(), value);
    }
}
