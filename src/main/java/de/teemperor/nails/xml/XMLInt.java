package de.teemperor.nails.xml;

public class XMLInt extends NamedXMLObject {

    private int value;

    public XMLInt(String name, int value) {
        super(name);
        this.value = value;
    }

    public void setValue(int newValue) {
        value = newValue;
    }

    @Override
    public String toXML() {

        return String.format("<int name=\"%s\">%d</int>", getName(), value);
    }
}
