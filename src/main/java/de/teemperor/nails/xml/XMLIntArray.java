package de.teemperor.nails.xml;

public class XMLIntArray extends NamedXMLObject {

    private int[] data;

    public XMLIntArray(String name, int[] data) {
        super(name);
        this.data = data.clone();
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data.clone();
    }

    @Override
    public String toXML() {
        String xmlString = "<int-arr name=\"" + getName() + "\">";
        for (int d : data) {
            xmlString += d + " ";
        }
        xmlString = xmlString.trim();
        return xmlString + "</int-arr>";
    }
}
