package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.Level;
import de.teemperor.nails.xml.XMLFloat;
import de.teemperor.nails.xml.XMLBool;
import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLObject;
import de.teemperor.nails.xml.XMLIntArray;
import de.teemperor.nails.xml.XMLString;
import java.util.ArrayList;

public class ScriptNode extends XMLObject {

    public enum NodeType {

        ToggleElement("ToggleElement"), AreaTrigger("AreaTrigger"), RespawnPlayers(
                "RespawnPlayers"), ShopArea("ShopArea"), LevelStart(
                        "LevelStart"), LevelExit("LevelExitArea"), AnnounceText(
                        "AnnounceText"), ObjectEventTrigger("ObjectEventTrigger"), RectangleShape(
                        "RectangleShape");

        public XMLString type;

        NodeType(String name) {

            this.type = new XMLString("type", name);
        }
    }

    private final int id;
    private final boolean enabled;
    private final int triggerTimes;
    private final float x;
    private final float y;
    private final NodeType type;
    private final ArrayList<ScriptNode> connections;

    public ScriptNode(float x, float y, NodeType type) {
        connections = new ArrayList<>();
        this.x = x;
        this.y = y;
        this.type = type;
        this.triggerTimes = -1;
        this.enabled = true;
        this.id = Level.idCounter++;
    }

    public void connectTo(ScriptNode n) {

        connections.add(n);
    }

    protected XMLDictionary getParametersDict() {

        return new XMLDictionary("parameters");
    }

    public int getID() {
        return id;
    }

    @Override
    public String toXML() {

        // create XML structure
        XMLInt idInt = new XMLInt("id", id);
        XMLString typeString = type.type;
        XMLBool enabledBool = new XMLBool("enabled", enabled);
        XMLInt triggerInt = new XMLInt("trigger-times", triggerTimes);
        XMLFloat xFloat = new XMLFloat("x", x);
        XMLFloat yFloat = new XMLFloat("y", y);
        XMLDictionary parameters = getParametersDict();

        XMLDictionary nodeDict = new XMLDictionary("");
        nodeDict.addData(idInt);
        nodeDict.addData(typeString);
        nodeDict.addData(enabledBool);
        nodeDict.addData(triggerInt);
        nodeDict.addData(xFloat);
        nodeDict.addData(yFloat);
        nodeDict.addData(parameters);

        if (!connections.isEmpty()) {

            // create array
            int[] ids = new int[connections.size()];
            for (int i = 0; i < ids.length; i++) {

                ids[i] = connections.get(i).id;
            }
            XMLIntArray conArray = new XMLIntArray("connections", ids);
            XMLIntArray delayArray = new XMLIntArray("delays", ids);
            nodeDict.addData(conArray);
            nodeDict.addData(delayArray);
        }

        return nodeDict.toXML();
    }
}
