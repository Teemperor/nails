package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.xml.XMLFloat;
import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;

public class NodeRectangleShape extends ScriptNode {

    private final float width = 1.0f;
    private final float height = 1.0f;
    private final int types = 15;

    public NodeRectangleShape(float x, float y, NodeType type) {
        super(x, y, type);
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLFloat("w", width));
        d.addData(new XMLFloat("h", height));
        d.addData(new XMLInt("types", types));

        return d;
    }
}
