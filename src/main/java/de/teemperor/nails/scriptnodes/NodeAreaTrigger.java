package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLIntArray;

public class NodeAreaTrigger extends ScriptNode {

    private final int event = 0;
    private final int types = 1;
    private int shapeId = 0;

    public NodeAreaTrigger(float x, float y, NodeType type) {
        super(x, y, type);
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLInt("event", event));
        d.addData(new XMLInt("types", types));
        XMLDictionary shapeDict = new XMLDictionary("shape");
        int[] shapeArray = new int[1];
        shapeArray[0] = shapeId;
        shapeDict.addData(new XMLIntArray("static", shapeArray));
        d.addData(shapeDict);
        return d;
    }

    public void connectToShape(ScriptNode n) {
        shapeId = n.getID();
    }

}
