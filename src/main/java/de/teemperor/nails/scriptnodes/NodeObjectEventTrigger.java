package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.Item;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLIntArray;
import de.teemperor.nails.xml.XMLString;
import java.util.ArrayList;

public class NodeObjectEventTrigger extends ScriptNode {

    private final String event = "Destroyed";
    private final ArrayList<Item> itemConnections = new ArrayList<>();

    public NodeObjectEventTrigger(float x, float y, NodeType type) {
        super(x, y, type);
    }

    public void connectItem(Item i) {
        itemConnections.add(i);
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLString("event", event));

        XMLDictionary objectDict = new XMLDictionary("object");
        int[] array = new int[itemConnections.size()];
        for (int i = 0; i < array.length; i++) {

            array[ i] = itemConnections.get(i).getID();
        }
        objectDict.addData(new XMLIntArray("static", array));
        d.addData(objectDict);

        return d;
    }
}
