package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLIntArray;

public class NodeToggleElement extends ScriptNode {

    private int state = 1; //disable
    private int element = 0;

    public NodeToggleElement(float x, float y, NodeType type) {
        super(x, y, type);
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLInt("state", state));
        XMLDictionary eDict = new XMLDictionary("element");
        int[] shapeArray = new int[1];
        shapeArray[0] = element;
        eDict.addData(new XMLIntArray("static", shapeArray));
        d.addData(eDict);
        return d;
    }

    public void connectToElement(ScriptNode n) {
        element = n.getID();
    }

}
