package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.Level;
import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLIntArray;
import de.teemperor.nails.xml.XMLString;

public class NodeLevelExit extends ScriptNode {

    private int level;
    private int startId = 0;
    private int shapeId = 0;

    public NodeLevelExit(float x, float y, NodeType type) {
        super(x, y, type);

        level = Level.currentLevel + 1;
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLString("level", String.format("%d", level)));
        d.addData(new XMLInt("start id", startId));
        XMLDictionary shapeDict = new XMLDictionary("shape");
        int[] shapeArray = new int[1];
        shapeArray[0] = shapeId;
        shapeDict.addData(new XMLIntArray("static", shapeArray));
        d.addData(shapeDict);
        return d;
    }

    public void connectToShape(ScriptNode n) {
        shapeId = n.getID();
    }

}
