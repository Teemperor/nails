package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;
import de.teemperor.nails.xml.XMLString;

public class NodeAnnounceText extends ScriptNode {

    private String text = "You win!!!";
    private final int time = 10000;
    private final int textType = 0;

    public NodeAnnounceText(float x, float y, NodeType type) {
        super(x, y, type);
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLString("text", text));
        d.addData(new XMLInt("time", time));
        d.addData(new XMLInt("type", textType));

        return d;
    }

    public void setText(String newText, int type) {
        text = newText;
    }
}
