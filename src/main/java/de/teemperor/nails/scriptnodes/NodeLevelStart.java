package de.teemperor.nails.scriptnodes;

import de.teemperor.nails.xml.XMLInt;
import de.teemperor.nails.xml.XMLDictionary;

public class NodeLevelStart extends ScriptNode {

    private final int pId = 0;
    private final int pDir = 2;

    public NodeLevelStart(float x, float y, NodeType type) {
        super(x, y, type);
    }

    @Override
    protected XMLDictionary getParametersDict() {

        XMLDictionary d = new XMLDictionary("parameters");
        d.addData(new XMLInt("id", pId));
        d.addData(new XMLInt("dir", pDir));
        return d;
    }
}
